import GradeResult from "../component/GradeResult";
import {useState} from "react" ;
function GradeConsiderPage() {
    const [name,setName] = useState("") ;
    const [gradeResult,setGradeResult] = useState(0) ;
    const [translateResult,setTranslateResult] = useState("") ;
    const [score,setScore] = useState("") ;
    function calculateGrade(){
        let s = parseInt(score) ;
        setGradeResult(s) ;
        if (s < 50){
            setTranslateResult("E");
        }else if (s >= 50) && (s <= 54)  {
            setTranslateResult("D");
        }else if (s >= 55) && (s <= 59) {
            setTranslateResult("D+");
        }else if (s >= 60) && (s <= 64) {
            setTranslateResult("C");
        }else if (s >= 65) && (s <= 69) {
            setTranslateResult("C+");
        }else if (s >= 70) && (s <= 74)   {
            setTranslateResult("B");
        }else if (s >= 75) && (s <= 79)  {
            setTranslateResult("B+");
        }else {
            setTranslateResult("A");
        }
    }
return(
<div align = "left">
    <div align = "center">
University Scholar Grade
    <hr />
คุณชื่อ : <input type = "text" 
               value = {name}
               onChange = { (e) => {setName(e.target.value) ;} }/> <br />
คะแนนที่ได้ : <input type = "text" 
value = {score}
onChange = { (e) => { setScore(e.target.value) ;} }/> <br />
<button onClick={() => {calculateGrade()}}>Calculate</button>
{ GradeResult != 0 &&
<div>
<hr />
เกรดที่ได้
<GradeResult 
Name = {name}
Score  = {gradeResult}
Grade = {translateResult}
/>
</div>
}
</div>
</div>
);
}
export default GradeConsiderPage;