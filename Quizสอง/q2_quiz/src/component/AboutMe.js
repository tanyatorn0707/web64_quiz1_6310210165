function AboutMe (props) {
    return(
        <div>
            <h3>จัดทำโดย = {props.Name} </h3>
            <h3>นักศึกษาชั้นปีที่ {props.Year} </h3>
            <h3>คณะ = {props.Faculty}</h3>
            <h3>สาขา = {props.Department}</h3>
            <h3>ติดต่อธัญธร = {props.PhoneNumber}</h3>
            <h3>Facebook = {props.Account}</h3>
        </div>
    );
}
export default AboutMe;