import logo from './logo.svg';
import './App.css';

import AboutMe from './component/AboutMe' ;
import GradeResult from './component/GradeResult';

function App() {
  return (
    <div className="App">
      <AboutMe Name = "Tanyatorn Lee-lerdlum" 
               Year = "2"
               Faculty = "Science"
               Department = "Computer Science"
               PhoneNumber = "087-4745123"
               Account = "Tanyatorn Lee-lerdlum"/>
      <hr/>
      <GradeResult Name = "Tanyatorn Lee-lerdlum" 
                 Lucky = "70"
                 Result = "B"/>
    </div>
  );
}

export default App;
